module Hlist : sig
  type _ t =
    | [] : unit t
    | ( :: ) : 'a * 'b t -> ('a * 'b) t

  type (_, _) nth =
    | Hd : (('a * 'b), 'a) nth
    | Tl : ('b, 'c) nth -> (('a * 'b), 'c) nth

  val nth : 'a t -> ('a, 'b) nth -> 'b

end

type ('acc, 'finish) k =
  | K of 'acc
  | Finish of 'finish

type _ ty =
  | [] : unit Hlist.t ty
  | ( :: ) : 'a ty * 'b Hlist.t ty -> ('a * 'b) Hlist.t ty
  | Uint8 : int ty
  | Int8 : int ty
  | String : int -> string ty
  | Bind : {
    header_ty: 'header ty;
    data_ty_maker : ('header -> 'data ty);
    header_maker : ('data -> 'header);
    sizer : ('header -> int);
  } -> 'data ty
  | Fold : {
    read_init : 'acc;
    reducer : ('acc -> ('elem -> 'acc, 'data) k);
    elem_ty : 'elem ty;
    exploder : ('data -> 'elem Seq.t);
  } -> 'data ty
  | Map : {
    blob_ty: 'a ty;
    serialising_conv: ('b -> 'a);
    deserialising_conv: ('a -> 'b);
  } -> 'b ty

val to_string : 'a ty -> 'a -> string

val of_string : 'a ty -> string -> 'a

val of_string_with_nth : 'a Hlist.t ty -> ('a, 'b) Hlist.nth -> string -> 'b
