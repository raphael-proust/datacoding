module Hlist = struct
  type _ t =
    | [] : unit t
    | ( :: ) : 'a * 'b t -> ('a * 'b) t

  type (_, _) nth =
    | Hd : (('a * 'b), 'a) nth
    | Tl : ('b, 'c) nth -> (('a * 'b), 'c) nth

  let rec nth
  : type a b. a t -> (a, b) nth -> b
  = fun t n ->
    match t, n with
    | x::_, Hd -> x
    | _::xs, Tl n -> nth xs n
    | _ -> .

  let () = assert ( 0 = nth [0; 2; 'a'; "foo"] Hd)
  let () = assert ( 2 = nth [0; 2; 'a'; "foo"] (Tl Hd))
  let () = assert ( 'a' = nth [0; 2; 'a'; "foo"] (Tl (Tl Hd)))
  let () = assert ( "foo" = nth [0; 2; 'a'; "foo"] (Tl (Tl (Tl Hd))))
end

type ('step, 'finish) k =
  | K of 'step
  | Finish of 'finish

type (_, _) eq = Eq : ('a, 'a) eq
module TyWit = struct
  type 'a witness = ..

  module type TyWit = sig
    type t

    val witness : t witness

    val eq : 'a witness -> ('a, t) eq option
  end

  type 'a id = (module TyWit with type t = 'a)

  let new_id (type a) () =
    let module TyWit = struct
      type t = a

      type 'a witness += TyWit : t witness

      let witness = TyWit

      let eq (type b) : b witness -> (b, t) eq option = function
        | TyWit -> Some Eq
        | _ -> None
    end in
    (module TyWit : TyWit with type t = a)

  let eq : type a b. a id -> b id -> (a, b) eq option =
   fun (module TyWitA) (module TyWitB) -> TyWitB.eq TyWitA.witness
end


module Hunion (C : sig type 'a case end) = struct
  (* In this module, the documentation comment use the running example of the
     following type:

     ```
     type t =
       | Foo of int
       | Bar of string
       | Baz of string
     ```

  *)

  (* a case is _one_ variant of a variant type, the c_id field is the runtime
     tag for that variant

     For exmple, `Foo of int` would be represented as an `int case`. The runtime
     tag (c_id) helps differentiate tags with the same paylod type (e.g., the
     two `string case` for the two variants `Bar of string` and `Baz of string`.
  *)
  type 'a case = {
    case : 'a C.case;
    c_id : 'a TyWit.id;
  }
  let case case = { case; c_id = TyWit.new_id () }

  (* a union is a variant type: a collection of cases, the u_id field is a
     runtime tag to ensure that we can't mix-up type-compatible unions

     For example, the type t (above) would be a
     `(int * (string * (string * empty))) union` and it would be constructed
     with `t_union = union [foo_case; bar_case; baz_case]`.
  *)
  type empty = |
  type _ cases =
    | [] : empty cases
    | ( :: ) : 'a case * 'b cases -> ('a * 'b) cases
  type 'a union = {
    cases: 'a cases;
    u_id: 'a TyWit.id;
  }
  let union cases = { cases; u_id = TyWit.new_id () }

  (* a `β oneof` is a value of a union, the type β is the product type of all
     the possible types of the union

     For example `Foo 3` is represented as
     `{ union = t_union.u_id; case = foo_case.c_id; v = 3}`. The function
       `construct` (below) helps construct those values. In fact, the type
       `oneof` is meant to be abstract outside of this module. *)
  type _ oneof = OneOf : { union: 'b TyWit.id; case: 'a TyWit.id; v: 'a } -> 'b oneof

  (* a `(β, α) accessor` is the constructor/destructor for the an `α case` of a
     `β union`

     Think of an accessor as `Foo`, or `Bar`, or `Baz`. They can be used to
     construct values (e.g., `Foo 3` constructs a value of type `t`, it
     "injects" `3` into the union `t`). They can also be used to destrcut values
     (e.g., `| Foo n -> …` executes code after extracting the payload carried by
     the injected value). *)
  type ('b, 'a) accessor =
    | Hd : (('a * 'b), 'a) accessor
    | Tl : ('b, 'c) accessor -> (('a * 'b), 'c) accessor

  (* finds the case of a union that corresponds to a given accessor *)
  let rec case_of_cases
  : type a b. (b, a) accessor -> b cases -> a case
  = fun d cs ->
    match d with
    | Hd -> begin match cs with | c :: _ -> c end
    | Tl d -> begin match cs with | _ :: cs -> case_of_cases d cs end

  (* construct a union value: takes a payload (think an `int` for `Foo`) and an
     accessor (think `Foo`) and makes a `oneof` (think `Foo 3`) *)
  let construct
  : type a b. b union -> (b, a) accessor -> a -> b oneof
  = fun u d x ->
    let case = case_of_cases d u.cases in
    OneOf { union = u.u_id; case = case.c_id; v=x }

  (* destruct a union value: takes a union value and an accessor and returns the
     payload (or None) if there's a variant/case mismatch (see Projectors below
     for general matching) *)
  let destruct
  : type a b. b union -> (b, a) accessor -> b oneof -> a option
  = fun u d (OneOf o) ->
    match TyWit.eq u.u_id o.union with
    | None -> None
    | Some Eq ->
        let rec destruct
        : type a b. b cases -> (b, a) accessor -> a option
        = fun cs d ->
          match d with
          | Hd -> begin
            match cs with
            | c::_ ->
              match TyWit.eq c.c_id o.case with
              | None -> None
              | Some Eq -> Some o.v
          end
          | Tl d -> begin
            match cs with
            | _ :: cs -> destruct cs d
          end
        in
        destruct u.cases d

  (* Projectors are a more generic version of destructors. Instead of
     destructing one single case, they can destruct from any case. To that end a
     projector contains conversion function for each of the cases in the
     union.

     The accessor `Hd` is equivalent to the projector
     `[(_, Option.some); (_, (fun _ -> None)); (_, (fun _ -> None)) …] with the
     correctly set ids and the correct number of elements.

     The expression
     ```
     match u with
     | Foo n -> n
     | Bar s -> String.length s
     | Baz _ -> 0
     ```
     is equivalent to the expression
     ```
     project
       [
         (foo_case.c_id, (fun n -> n));
         (bar_case.c_id, (fun s -> String.length s));
         (baz_case.c_id, (fun _ -> 0));
       ]
       u
     ```
  *)
  module Projector = struct
    type (_, _) projector =
      | [] : (empty, 'b) projector
      | ( :: ) : ('a TyWit.id * ('a -> 'b)) * ('c, 'b) projector -> (('a * 'c), 'b) projector
  end
  let project
  : type a b. (a, b) Projector.projector -> a oneof -> b option
  = fun p (OneOf o) ->
    let rec project
    : type a b. (a, b) Projector.projector -> b option
    = fun p ->
      match p with
      | [] -> None
      | (id, f) :: p ->
          match TyWit.eq id o.case with
          | None -> project p
          | Some Eq -> Some (f o.v)
    in
    project p

end

module Hunion_asserts = Hunion(struct type 'a case = unit end)
let () =
  let open Hunion_asserts in
  (* declaring cases and then a union *)
  let a_case : int case = case () in
  let b_case : int case = case () in
  let c_case : string case = case () in
  let u : (int * (int * (string * empty))) union = union [ a_case; b_case; c_case ] in
  (* constructing an A *)
  let a1 = construct u Hd 1 in
  (* checking the destructions return Some/None as expected *)
  assert (destruct u Hd a1 = Some 1);
  assert (destruct u (Tl Hd) a1 = None);
  assert (destruct u (Tl (Tl Hd)) a1 = None);
  (* constructing and destructing more values *)
  let a2 = construct u Hd 2 in
  assert (destruct u Hd a2 = Some 2);
  let b2 = construct u (Tl Hd) 2 in
  assert (destruct u (Tl Hd) b2 = Some 2);
  assert (destruct u Hd b2 = None);
  let cfoo = construct u (Tl (Tl Hd)) "foo" in
  assert (destruct u (Tl (Tl Hd)) cfoo = Some "foo");
  (* projecting onto Either *)
  let p
    : ((int * (int * (string * empty))), (int, string) Either.t) Projector.projector
    = Projector.[
        (a_case.c_id, Either.left);
        (b_case.c_id, Either.left);
        (c_case.c_id, Either.right);
    ]
  in
  (* checking that projection works *)
  assert (project p a1 = Some(Either.Left 1));
  assert (project p b2 =  Some(Either.Left 2));
  assert (project p cfoo =  Some(Either.Right "foo"));
  ()

module Ty = struct

(* TODO: make string parametric so we can either return a fresh one or an
   index of a substring when reading *)
type _ ty =
  | [] : unit Hlist.t ty
  | ( :: ) : 'a ty * 'b Hlist.t ty -> ('a * 'b) Hlist.t ty
  | Uint8 : int ty
  | Int8 : int ty
  | String : int -> string ty
  | Bind : {
    header_ty: 'header ty;
    data_ty_maker : ('header -> 'data ty);
    header_maker : ('data -> 'header);
    sizer : ('header -> int);
  } -> 'data ty
  | Fold : {
    read_init : 'acc;
    reducer : ('acc -> ('elem -> 'acc, 'data) k);
    elem_ty : 'elem ty;
    exploder : ('data -> 'elem Seq.t);
  } -> 'data ty
  | Map : {
    blob_ty: 'a ty;
    serialising_conv: ('b -> 'a);
    deserialising_conv: ('a -> 'b);
  } -> 'b ty

let rec size_static
: type a. a ty -> int option
=
  let ( let* ) = Option.bind in
  fun ty ->
  match ty with
  | [] -> Some 0
  | ty::tys ->
      let* s = size_static ty in
      let* ss = size_static tys in
      Some (s + ss)
  | Uint8 -> Some 1
  | Int8 -> Some 1
  | String n -> Some n
  | Bind _ -> None
  | Fold _ -> None
  | Map { blob_ty; _ } -> size_static blob_ty

let sized
: type a. (a -> int) -> (int -> a ty) -> a ty
= fun sizeof mkty ->
  Bind {
    header_ty = Uint8;
    data_ty_maker = mkty;
    header_maker = sizeof;
    sizer = Fun.id;
  }

let dynstring : string ty =
  sized String.length (fun n -> String n)

let list ty =
  match size_static ty with
  | Some n ->
      Bind {
        header_ty = Uint8 ;
        data_ty_maker =
          (fun len ->
            Fold {
              read_init = (len, List.[]);
              reducer =
                (fun (len, acc) ->
                  if len = 0 then
                    Finish (List.rev acc)
                  else
                    K (fun v -> (len - 1, v :: acc)));
              elem_ty = ty;
              exploder = List.to_seq ;
            });
        header_maker = List.length;
        sizer = (fun h -> h * n);
      }
  | None ->
      assert false

let dynlist sizeof mkty =
  Bind {
    header_ty = [Uint8; Uint8] ;
    data_ty_maker =
      (fun Hlist.[len; _total_size] ->
        Fold {
          read_init = (len, List.[]);
          reducer =
            (fun (len, acc) ->
              if len = 0 then
                Finish (List.rev acc)
              else
                K (fun v -> (len - 1, v :: acc)));
          elem_ty = sized sizeof mkty ;
          exploder = List.to_seq ;
        });
    header_maker =
      (fun xs -> Hlist.[
        List.length xs;
        List.fold_left (fun acc x -> acc + 1 + sizeof x) 0 xs;
      ]);
    sizer = (fun Hlist.[_len; total_size] -> total_size);
  }

let dynstringlist =
  dynlist String.length (fun n -> String n)

let either tya tyb =
  match size_static tya, size_static tyb with
  | Some sa, Some sb ->
      Bind {
        header_ty = Uint8;
        data_ty_maker =
          (function
            | 0 ->
                Map {
                  blob_ty = tya;
                  serialising_conv = (function Either.Left v -> v | _ -> assert false) ;
                  deserialising_conv = (fun v -> Either.Left v);
                }
            | 1 ->
                Map {
                  blob_ty = tyb;
                  serialising_conv = (function Either.Right v -> v | _ -> assert false) ;
                  deserialising_conv = (fun v -> Either.Right v);
                }
            | _ -> assert false);
        header_maker = (function Either.Left _ -> 0 | Either.Right _ -> 1);
        sizer = (function 0 -> sa | 1 -> sb | _ -> assert false);
      }
  | _ -> assert false

end

include Ty

let rec to_string
: type a. a ty -> a -> string
= fun ty x ->
  match ty with
  | [] -> assert (x = Hlist.[]); ""
  | ty::tys -> begin
    match x with
    | x :: xs ->
        let s = to_string ty x in
        let ss = to_string tys xs in
        s ^ ss
  end
  | Uint8 ->
      assert (0 <= x);
      assert (x <= 255);
      String.make 1 (Char.chr x)
  | Int8 ->
      assert ((-128) <= x);
      assert (x <= 127);
      String.make 1 (Char.chr (x + 128))
  | String n ->
      assert (String.length x = n);
      x
  | Bind { header_ty; data_ty_maker; header_maker; sizer; } ->
      let header = header_maker x in
      let s_header = to_string header_ty header in
      let data_ty = data_ty_maker header in
      let sx = to_string data_ty x in
      assert (String.length sx = sizer header);
      s_header ^ sx
  | Fold  { read_init = _; reducer = _; elem_ty; exploder } ->
      String.concat "" @@ List.of_seq @@ Seq.map (to_string elem_ty) @@ exploder x
  | Map { blob_ty; serialising_conv; deserialising_conv=_;} ->
      to_string blob_ty (serialising_conv x)

let rec of_string_with_offset
: type a. a ty -> int -> string -> (a * int)
= fun ty offset blob ->
  match ty with
  | [] -> (Hlist.[], offset)
  | ty::tys ->
    let (x, offset) = of_string_with_offset ty offset blob in
    let (xs, offset) = of_string_with_offset tys offset blob in
    (Hlist.(x :: xs), offset)
  | Uint8 ->
      let x = Char.code @@ String.get blob offset in
      (x, offset+1)
  | Int8 ->
      let x = Char.code @@ String.get blob offset in
      (x-128, offset+1)
  | String n ->
      let x = String.sub blob offset n in
      let offset = offset + n in
      (x, offset)
  | Bind { header_ty; data_ty_maker; header_maker=_; sizer; } ->
      let (header, offset) = of_string_with_offset header_ty offset blob in
      let start_offset = offset in
      let data_ty = data_ty_maker header in
      let (_, offset) as r = of_string_with_offset data_ty offset blob in
      assert (offset - start_offset = sizer header);
      r
  | Fold {read_init; reducer; elem_ty; exploder=_; } ->
      let rec reduce acc offset =
        match reducer acc with
        | K k ->
            let (elem, offset) = of_string_with_offset elem_ty offset blob in
            let acc = k elem in
            reduce acc offset
        | Finish f -> (f, offset)
      in
      reduce read_init offset
  | Map { blob_ty; serialising_conv=_; deserialising_conv; } ->
      let (pre, offset) = of_string_with_offset blob_ty offset blob in
      (deserialising_conv pre, offset)

let of_string ty blob = fst @@ of_string_with_offset ty 0 blob

let rec size_with_blob
: type a. a ty -> string -> int -> int
= fun ty blob offset ->
  match ty with
  | [] -> 0
  | ty :: tys ->
      let s = size_with_blob ty blob offset in
      let ss = size_with_blob tys blob (offset + s) in
      s + ss
  | Uint8 -> 1
  | Int8 -> 1
  | String n -> n
  | Bind { header_ty; sizer; _ } ->
      let old_offset = offset in
      let (header, offset) = of_string_with_offset header_ty offset blob in
      sizer header + (offset - old_offset)
  | Fold _ ->
      assert false
  | Map { blob_ty; _ } ->
      size_with_blob blob_ty blob offset


let ty : int list ty = Ty.list Uint8

let test xs =
  let s = to_string ty xs in
  let ys = of_string ty s in
  (xs = ys)

let () = assert (test [])
let () = assert (test [1])
let () = assert (test [1;2;3])
let () = assert (test [1;3;100;2;3;4;5;5;5;5;5;5;5;5;5;5;5;])

let oty
: (int * (string * (int list * (int * ((int, string) Either.t * unit))))) Hlist.t ty
= [ Uint8; dynstring; ty; Int8; either Uint8 (String 3)]

let otest d =
  let s = to_string oty d in
  let dd = of_string oty s in
  (d = dd)

let () = assert (otest [120; "foo"; List.[]; (-1); Either.Left 14])
let () = assert (otest [120; "foobar"; List.[1;2;3;4]; (-1); Either.Right "foo"])
let () = assert (otest [0; ""; List.[0]; (-1); Either.Right "bar"])

let rec of_string_with_nth
: type a b. a Hlist.t ty -> (a, b) Hlist.nth -> int -> string -> b
= fun ty nth offset blob ->
  match ty, nth with
  | Ty.(ty :: _), Hd ->
        let (x, _) = of_string_with_offset ty offset blob in
        x
  | Ty.(ty :: tys), Tl nth ->
      let s = size_with_blob ty blob offset in
      of_string_with_nth tys nth (offset + s) blob
  | ty, nth ->
      let xs, _ = of_string_with_offset ty offset blob in
      Hlist.nth xs nth

let of_string_with_nth ty nth blob = of_string_with_nth ty nth 0 blob


(* ty that trips an exception to prove that `nth` skips without decoding *)
let tripty : int ty =
  Map {
    blob_ty = Uint8;
    serialising_conv = Fun.id;
    deserialising_conv = (fun _ -> assert false);
  }
let aty
: (int * (string * (int list * (int * (int * unit))))) Hlist.t ty
= [ tripty; dynstring; list tripty; Int8; tripty]

let atest d =
  let s = to_string aty d in
  let dd = of_string_with_nth oty (Tl (Tl Hd)) s in
  let ddd = Hlist.nth d (Tl (Tl Hd)) in
  let dd2 = of_string_with_nth oty (Tl (Tl (Tl Hd))) s in
  let ddd2 = Hlist.nth d (Tl (Tl (Tl Hd))) in
  (dd = ddd) && (dd2 = ddd2)

let () = assert (atest [120; "foobarbaz"; List.[]; (-1); 11])
let () = assert (atest [120; "floflarflaz"; List.[1;2;3;4]; 1; 14])

let dty
: (string list * (string list * unit)) Hlist.t ty
= [ dynstringlist; list (String 3) ]

let dtest d =
  let s = to_string dty d in
  let dd = of_string dty s in
  (d = dd)

let () = assert (dtest [[]; []])
let () = assert (dtest [[]; ["foo"; "bar"]])
let () = assert (dtest [["lol"; "looooool"; "loooooooooooooooooool"]; []])
let () = assert (dtest [["lol"; "looooool"; "loooooooooooooooooool"]; ["baz"]])
let () = assert (dtest [["lol"]; ["baz"]])
